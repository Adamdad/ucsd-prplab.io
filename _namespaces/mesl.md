---
layout: namespace
title: mesl
date: 2019-05-29
name: mesl
pi: Rajesh Gupta
institution: University of California, San Diego
software: Python, Cuda, OpenAI gym, Conda, TensorBoard, Keras, stable-baseline
tagline: Convolutional Neural Networks and Reinforcement Learning 
imagesrc: mesl.png
categories: 
- "namespace" 
tags: []
---

<b>Project 1: DeepQuery</b><br>
Mobile offloading framework for CNN tasks. DeepQuery serves many tasks such as object detection and tracking, and optimally schedules heavy loads among many devices
Software: Python, Cuda, Conda, Tensorflow, Tensorboard
<br>
<b>Project 2: DeepHVAC</b><br>
Buildings account ~40% of the energy consumption nation-wide, mostly by energy-hogging air conditioning (AC). There is a huge opportunity to optimize AC control algorithms, but it entails significant human effort such as physical modeling of the building, parameter optimization, and simulations. DeepHVAC is a data-driven optimization using reinforcement learning with learnt models.
Software: Python, Cuda, OpenAI gym, Conda, Tensorboard, Keras, stable-baseline

