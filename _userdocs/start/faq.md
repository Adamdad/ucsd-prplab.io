---
layout: collection-userdocs
title: FAQ
date: 2019-01-23
short: userdocs
categories: user
group: help
order: 60
---

1. How do i access nautilus?  See [Get access][1]
1. How do I use Kubernetes? See [Quick Start][2]
1. How do I use S3? See [Storage][3]
1. Where is Nautilus Located?<br>
Nautilus is a heterogeneous, distributed cluster, with computational resources of various shapes and 
sizes made available by research institutions spanning multiple continents! Check out the Cluster Map 
to see where the nodes are located. 
1. How do I get data into my ceph volume from outside?
    * Upload data to [S3][3] from outside, then use it from inside the pod
    * Upload data to [Nextcloud][4], then download from it to pod using WebDAV
    * [Exec][5] into pod and pull data to it using [scp][6], [rclone][7], wget, curl, etc...
    * For **really small** files, like a config file or a binary, you can use `kubectl cp` command which will send one through API server. This method is too slow and loads our master nodee too much to be used for large data transfers.
1. I'm getting `failed to refresh token`, `oauth2`, `server_error` errors when trying to access the cluster with kubectl.<br>
Get the config file again.
1. This happens too often, and I need to pull the config file over and over again.<br>
You're probably using kubectl concurrently (from several shells in parallel), which breaks the token update mechanism. [Consider using ServiceAccounts][8] for scripts.
1. My pod is stuck Terminating.<br>
This happens for 2 reasons:
    * The node running your pod went offline. The pod will get terminated once the node is back online
    * The storage attached to the pod can't be unmounted.<br>
In both cases you can ask an admin in rocketchat to look at your pod, or just wait for somebody to fix it.<br>
**DON'T USE kubectl  delete \-\-grace-period=0 \-\-force to delete stuck pods**

[1]: /userdocs/start/get-access/
[2]: /userdocs/start/quickstart/
[3]: /userdocs/storage/ceph/#ceph_s3
[4]: /userdocs/storage/nextcloud/
[5]: https://kubernetes.io/docs/tasks/debug-application-cluster/get-shell-running-container/
[6]: https://en.wikipedia.org/wiki/Secure_copy
[7]: https://rclone.org/
[8]: /userdocs/running/scripts/
