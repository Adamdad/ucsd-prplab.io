---
layout: namespace
title: kube-environment
date: 2019-05-29
name: kube-environment
pi: Bill Lin
institution: University of California, San Diego
software: PyTorch, Python, CUDA
tagline: ReRAM-based deep neural networks
imagesrc: kube-environment.png
categories: 
- "namespace" 
tags: []
---

Recent works have demonstrated the promise of using resistive random access memory (ReRAM) to perform neural network computations 
in memory.  In particular, ReRAM-based crossbar structures can perform matrix-vector multiplication directly in the analog domain, 
but the resolutions of ReRAM cells and digital/ analog converters limit the precisions of inputs and weights that can be directly 
supported.  In this project, we are developing a new CNN training and implementation approach that implements weights using a 
trained biased number representation, which can achieve near full-precision model accuracy with as little as 2-bit weights and 2-bit activations..
