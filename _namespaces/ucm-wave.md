---
layout: namespace
title: ucm-wave
date: 2019-05-29
name: ucm-wave
pi: Jeffrey Weekley
institution: University of California, Merced
software:  CGLX, CalVR
tagline: Virtual Reality
imagesrc: ucm-wave.png
categories: 
- "namespace" 
tags: []
---

UC Merced Wide-Area Visualization Environment (WAVE) runs large-scale visualization software and content on Nautilus. Visualizations such as this help 
researchers develop a deeper understanding of their work through critical viewing and user interactions.
