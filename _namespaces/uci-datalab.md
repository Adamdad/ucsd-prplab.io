---
layout: namespace
title: uci-datalab
date: 2019-05-17
name: uci-datalab
pi: Padhraic Smyth
institution: University of California, Irvine
software: Python, CUDA, PyTorch, NVIDIA Apex, AllenNLP
tagline: Knowledge Aware Language Modeling
imagesrc: uci-datalab.png
categories: 
- "namespace" 
tags: []
---

We are working on knowledge aware language modeling, which leverages structured knowledge (e.g., the WikiData knowledge graph) 
to help language model's better model factual information. By rendering statements from the knowledge source, knowledge aware 
language models have the potential to improve the quality of natural language generation systems such as dialogue agents. 
In addition, knowledge aware language models can also be used to learn pre-trained contextualized word representations that 
improve performance on downstream tasks such as relation extraction, natural language inference, and word sense disambiguation.
