---
layout: collection-admindocs
title: Builds 
date: 2019-04-04
short: admindocs
categories: user
order: 1
---

<div class="border">
<strong>Installation instructions </strong>
</div>
[FIONA DTN v2 on CentOS 7][fionadtn] 


<div class="border">
  <strong>Hardware specs</strong> 
</div>

Hardware specs and pricing of specific components.

| 2020 | 2019 | 2018 | 2017 | 2016 | 2015 |
| ---- | ---- | ---- | ---- | ---- |
|[Winter][2020w] | [Spring][2019sp] | [Fall][2018fa] | [Spring][2017su] | [Summer][2016su] | [Fall][2015fa] | 
|  | | [Spring][2018sp]  |  |  |

[2020w]: https://docs.google.com/spreadsheets/d/14BuHPraE_s3OeCl715s7YGQQUkopUmquz5oC_S0AjkM/edit#gid=599428996
[2019sp]: https://docs.google.com/spreadsheets/d/1tR9_MuDbo2xImXaAulUNRxKnB2VGEryWhlKT31kBgPE/edit#gid=914865956
[2018fa]: https://docs.google.com/spreadsheets/d/1b6EzbwMB36T9ndAmCsGMgcThJDSnVItcqj2RT6RwQ2o/edit#gid=1559789080
[2018sp]: https://docs.google.com/spreadsheets/d/11yhDhrYSf7tbq67a7FZJefpWr3RPUAnw5s3SAk6kIn4/edit#gid=1062495698
[2017su]: https://docs.google.com/spreadsheets/d/1wTdLzf1xLui1Dj0Gh_dGjfxxIsCWIrjvxggdR3Btlcg/edit#gid=1062495698
[2016su]: https://docs.google.com/spreadsheets/d/1mYYWpwcsAXREER-tiX1KKtpMuFm0hUCY02xggbsQCJM/edit#gid=147936727
[2015fa]: https://docs.google.com/spreadsheets/d/1z-FH4ueBTTEwhED4WYpeACHRTXdm2xN6TFI2ix5ppts/edit#gid=254071464
[fionadtn]: https://docs.google.com/document/d/1GZqFyAiyZC0SEIeE1TCDv8ypBezLFvsU5ebKuWH0FYk/edit
[maddash1]: https://perfsonar.nautilus.optiputer.net/maddash-webui/
[maddash2]: https://perfsonar.nrp-nautilus.io/maddash-webui/index.cgi?dashboard=NRP_GridFTP
[maddash3]: https://ps-dashboard.cenic.net/maddash-webui/
[esnet]: http://fasterdata.es.net
[caltechs]: http://supercomputing.caltech.edu/blog/
