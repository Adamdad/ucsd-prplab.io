---
layout: namespace
title: mas-fuelmap
date: 2019-05-22
name: mas-fuelmap
pi: Mai Nguyen
institution: University of California, San Diego
software: Keras, Scikit-learn, Spark, PIL, GDAL
tagline: Generating land cover and fuel maps from satellite imagery
imagesrc: mas-fuelmap.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

Land data products such as fuel maps and land cover maps are critical for many applications including land use analysis, 
bio-diversity conservation, and wildfire management.  Current products provide essential land data and are widely used by 
various agencies across the nation.   However, these products are generated very infrequently (e.g., every few years) 
and based on medium-resolution imagery that do not provide the granularity possible with high-resolution imagery.
<br><br>
Our research proposes an approach to generate products as needed, based on up-to-date imagery, and at scale. Specifically, 
our research goals are to generate more frequent products by creating maps as needed from satellite imagery, more accurate 
products by using up-to-date, high-resolution satellite imagery, and scalable products by utilizing machine learning to 
automate the process.  The approach we use extracts features from satellite images using a deep learning model.  The resulting 
feature vectors are then used for classifying or segmenting the images in order to generate land cover maps.  We plan to 
extend our work to multi-spectral imagery, larger and more varied regions, and other types of land data products.
