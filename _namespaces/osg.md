---
layout: namespace
title: osg
date: 2019-03-22
name: osg
pi: Frank Wuerthwein
institution: University of California, San Diego
software: XrootD
tagline: Cashing infrastructure
imagesrc: osg.png
categories: 
- "namespace" 
tags: [3D modeling]
---

<b>Caching technology deployed on Nautilus cluster: </b>Stashcache is a caching infrastructure based on the XrootD software. 
We deploy stashcache containers at grid sites and also in the Internet backbone.  The objective 
is to reduce latency for scientific datasets (open and private) that are accessed at several 
computing sites. At a computing site the "nearest" cache based on GeoIP is picked and accessed. 
<br>
<b>LIGO and the caching technology: </b>The LIGO experiment has computing resources located at several 
location in the US and above.  Moreover it can also access the VIRGO computing resources located in Europe. LIGO uses 
OSG powered technology glideinWMS to run workflows on its own computing resources, VIRGO 
resources and opportunistic resources. Given the distributed nature of its computing it needs 
to be able to securely access (Only members of the LIGO collaboration can access these datasets) 
its input data. The secure caching infrastructure deployed all over using
kubernetes provides this.
<br>Image credit: LIGO/T. Pyle
