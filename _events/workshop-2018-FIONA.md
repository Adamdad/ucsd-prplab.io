---
layout: page
title: FIONA Workshop 1
date: 2018-03-03
dates: 2018 March 3-4
location: Monteray, CA
siteimage: micro.png
categories:
- "workshop"
- "2018"
---

By the end of this workshop the participant will be able to:

- Understand selected use cases for using a FIONA/FIONette as a Data Transfer Node or Data Access Node
- Be able to deploy a FIONette node at their institution
- Install and configure perfSONAR tools including regular testing (throughput, latency / loss, traceroute)
- Install and configure GridFTP tools including regular disk-to-disk throughput testing
- Install and configure MaDDash visualization tool and Measurement Archive for storing test results
- Troubleshoot selected network issues using the pS toolkit and other tools
- Understand data movement performance analysis and visualization
- Understand scaling and tuning options for for optimal data flow, including
- BIOS / OS / FileSystem
- TCP/UDP network stack
- Single-stream vs Multiple-streams
- Larger flows
- Longer paths (with high Bandwidth Delay Products)- Properly secure FIONA/FIONette devices

<a href="http://prp.ucsd.edu/prpworkshop" class="button">event information page</a>
