---
layout: page
title: 3NRP Workshop 2019                   
date: 2019-9-24
dates: 2019 September 24-25
event_start_date: 20190924
location: Minneapolis, MN
eventurl: "https://meetings.internet2.edu/2019-924-third-national-research-platform-workshop/"
siteimage: events.png
imagesrc: 3nrp_date.png
categories: 
- "workshop"
- "2019"
tags: []
---

#### The Third National Research Platform Workshop - Minneapolis, MN

Building on the success of last year’s Second National Research Platform Workshop, the Third NRP Workshop will be held in Minneapolis, Minnesota on September 24-25, 2019 at the Renaissance Downtown Minneapolis – The Depot.
 
The NRP workshop will be co-located with the NSF PI workshop (9/23-9/25) and the Quilt meeting (9/25-9/26) with some shared sessions. A Global Research Platform (GRP) Workshop will be held the week prior to the NRP workshop and it will focus on international Research Platform topics. 
Information about the Third NRP Workshop, hotel/travel information and the co-located events can be found
<a href="https://meetings.internet2.edu/2019-924-third-national-research-platform-workshop/" class="sbutton" >here</a>.
