---
layout: namespace
title: words-geo
date: 2019-05-19
name: words-geo
pi: Daniel Crawl
institution: University of California, San Diego
software: GDAL, Apache HTTP
tagline: Satellite image processing
imagesrc: words-geo.png
categories: 
- "namespace" 
tags: []
---

GOES GeoColor is an imagery product from the new GOES satellites. Daytime imagery is RGB, and nighttime is 2 IR bands. 
Since GOES is geostationary and the refresh rate is every 5 minutes, GeoColor imagery is useful for monitoring wildfires 
and smoke plumes. We download new imagery, re-project, and split into tiles at different zoom levels to create a layer 
for the WIFIRE Firemap. We also plan to generate timelapse movies of the imagery.
<br>

See an example of a timelapse of the Camp and Woolsey fires (California, 2018): 
<a href="https://words-geo-www.nautilus.optiputer.net/GOES16-ABI-CONUS-GEOCOLOR/CA-Camp-daytime-2018.mp4">movie</a>

