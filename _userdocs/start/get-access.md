---
layout: collection-userdocs
title: Get access 
date: 2019-01-10
short: userdocs
categories: user
order: 1
---

To get an account on Pacific Research Platform kubernetes portal 
1. To get access to the PRP Nautilus cluster, do the following
   - Point your browser to the [PRP Nautilus portal][1]
   - On the portal page click on "Login" button at the top right corner <img class="" src="/images/userdocs/portal-login.png" width="400px">
   - You will be redirected to the "CILogon" page:<br> <img class="floatl" src="/images/userdocs/cilogon.png" >
     On this page, **select an Identity Provider** from the menu and **Click "Log On"** button to use your 
     existing credentials for the chosen provider to sign in.  The provider is one of :<br>
     (1) If your institution is using CILogon as a federated certification authority it will be in the menu, select the name of your institution 
     and use either a personal account or an institutional G-suite account.  This is usually  your institutional account name (email) and a password
     associated with it. 
     <br>(2) If your institution is not using CILogon, you can select "Google" <br><br>

1. After a successful authentication you will login on the portal. On first login you become a 
**guest** and your account will need to be validated by the **admin users**.  Any admin user can 
validate your guest account,  promote you to **user** and add your account to their **namespace**. You
need to be assigned to at least one namespace (usually  a group project but can be your new namespace).

1. To notify admin users, register at [Rocketchat][2],  and use the [support channel][3] 
to request that your account be upgraded from **guest** to **user**, and that you be added to a namespace
(existing for a project or new one).  To get access to a namespace, please contact its owner (usually email).

1. Once you are granted the **user** role in the cluster and are added to the
namespace, you will get access to all namespace resources. 

1. If you're starting a new project and would like to have your own namespace,
either for yourself or for your group, you can request to be  promoted to the **admin**. 
This will give you permission to create any number of namespaces and invite other users to your namespace(s). 
Please note, you'll be the one responsible for all activity happening in your namespaces.

1. Follow [quick start][4] page to star using kubernetes.


[1]: https://nautilus.optiputer.net
[2]: https://rocket.nautilus.optiputer.net 
[3]: https://rocket.nautilus.optiputer.net/channel/general
[4]: /userdocs/start/quickstart/
