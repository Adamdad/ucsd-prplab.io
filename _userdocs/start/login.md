---
layout: collection-userdocs
title: Nautilus Login 
date: 2019-01-10
short: userdocs
categories: user
order: 0
---

#### This is a test page for buttons 

1. Login to [PRP Nautilus portal][2]  and click the **Get Config** link 
on top right corner of the page to get your configuration file

<a class="nbutton icon" href="https://nautilus.optiputer.net"><span>Nautilus login</span></a>
<a class="nbutton icon" href="#"><span>Logout</span></a>
<a class="nbutton icon" href="#"><span>some.user@email.address</span></a>

[2]: https://nautilus.optiputer.net

