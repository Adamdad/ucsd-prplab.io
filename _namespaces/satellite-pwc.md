---
layout: namespace
title: satellite-pwc
date: 2019-05-22
name: satellite-pwc
pi: Mai Nguyen
institution: University of California, San Diego
software: Caffe, Scikit-learn, Spark, GDAL
tagline: Demographic analysis of satellite imagery using machine learning
imagesrc: satellite-pwc.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

High resolution satellite imagery is a growing source of data with significant efficiency and cost benefits for evaluating the 
socio-economic demographics of neighborhoods. While the benefits of satellite imagery has created a lot of excitement, a new 
challenge is that the volume of high resolution satellite imagery is computationally expensive. To alleviate the volume of 
"Big Image Data", machine learning is a promising tool to automatically sift through and retrieve relevant image.  In this 
research, we present a framework for organizing collections of satellite images into demographically relevant categories using 
unsupervised learning techniques. Our framework first extracts features using pre-trained convolutional neural networks from 
tiles of high resolution satellite images of a city. Cluster analysis is then applied to these features to organize images 
into visually similar groups. The resulting image clusters are visualized in our customized web interface to enable demographers, 
social scientists, and economists to understand the organization of a city.

