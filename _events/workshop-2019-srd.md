---
layout: page
title:  Securing Research Data                 
date: 2019-08-08
dates: 2019 August 22
event_start_date: 20190822
location: Santa Clara, CA
siteimage: srd_banner.png
imagesrc: srd.jpg
categories: 
- "workshop"
- "2019"
tags: []
---

#### August 22, 2019 - Santa Clara, CA

Researchers, computer scientists and data scientists from a range of disciplines are working with increasingly large, multi-dimensional datasets in the context of complex networks and computing infrastructure. Data generated by these advanced applications are often subject to an evolving landscape of protection requirements, due to the involvement of human subjects or critical physical infrastructure. At the same time, collaborative research projects and teams drive demand for large-scale distributed platforms that can move data and compute securely over high-speed networks and cyberinfrastructure. How do we secure the data, the network, and the computer to meet this challenge?
This one-day workshop will convene researchers from a variety of disciplines who are leading these trends through their work in biology and medicine, public health and the social sciences, and other fields. In addition, IT service providers and security architects will describe new services and platforms being developed to provide secure research data management and computation capabilities at scale, while managing challenges of authentication, access control, and privacy protection. We will also present advanced theory on the tradeoffs between engineering systems for complexity and robustness, and how this tradeoff in itself can create fragility.
<br>
<b>Agenda</b>
<iframe src="https://drive.google.com/file/d/1mImafgsjn4UDrCRIe_2Id4llzNZRf61U/preview" width="640" height="480"></iframe>
<br>
<b>Speaker Bios</b>
<iframe src="https://drive.google.com/file/d/1XeJYObHItRCQS8EZY2amiWs-sSQAHmYb/preview" width="640" height="480"></iframe>
<b>Welcome - David Rusting</b>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/JhqDKs7YSB7aCR" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> 
<br>
<b>Keynote Address - Florence Hudson</b>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/IVpNIY4jdt8ytB" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> 
<br>
<b>Panel 1 - Rob Currie, Chief Technology Officer, UC Santa Cruz Genomics Institute Perspectives from domain scientists</b>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/pfNhxCoez4Tq7A" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe>
<br>
<b>Panel 1: Securing your research data: Perspectives from domain scientists - Hybridizing Kubernetes and HPC Securely by Pavan Gupta</b>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/h6n2tF7UPMCXyM" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> 
<br>
<b>It's not a bug...It's a feature: Secrets as a Service</b>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/pTGRQjDZYquzG9" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe>
<br>
<b>Keynote Address - David L. Alderson</b>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/MAYMdLcBwONzBt" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> 
<br>
<b>Panel 2 - Securing your infrastructure in a changing landscape: an engineering perspective</b>
<iframe src="//www.slideshare.net/slideshow/embed_code/key/okRtfUwsX9hGpf" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> 
<br>
<b>Panel 3: Security and Privacy in Practice - Sandeep Chandra, Executive Director of Sherlock Cloud; Director of Health Cyberinfrastructure Division, San Diego Supercomputer Center</b>
 <iframe src="//www.slideshare.net/slideshow/embed_code/key/7iQ4AXuwO3WShj" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> 
<br>
<a href="https://prp-secure.eventbrite.com/">Click here for more info</a>.

