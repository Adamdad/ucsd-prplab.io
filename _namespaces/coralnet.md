---
layout: namespace
title: coralnet
date: 2019-05-29
name: coralnet
pi: David Kriegman
institution: University of California, San Diego
software: Python, CUDA, Pytorch, Caffe
tagline: Computer Vision and Machine Learning for Coral Ecology
imagesrc: coralnet.png
categories: 
- "namespace" 
tags: []
---

Global and local stressors have caused a rapid decline of coral reefs across the world. To monitor the changes 
and take appropriate action large spatio-temporal surveys are needed. Data collection speeds are typically sufficient 
to meet this need but the subsequent image analysis remains slow as manual inspection of each photo is required. 
This creates a 'manual annotation bottleneck. CoralNet reduces this bottleneck by allowing modern computer vision 
algorithms based on deep learning to be deployed alongside human experts.  Deployed as a web site and service, coralnet.ucsd.edu
is a platform for collaboration and sharing of data.
