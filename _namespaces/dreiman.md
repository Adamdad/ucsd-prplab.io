---
layout: namespace
title: dreiman
date: 2019-05-30
name: dreiman
pi: David Reiman
institution: University of California, Santa Cruz
software: Python, TensorFlow, PyTorch
tagline: Deep learning research with applications in cosmology, extragalactic astronomy and astrophysics
imagesrc: dreiman.png
categories: 
- "namespace" 
tags: []
---

Deep learning research with applications in cosmology, extragalactic astronomy and astrophysics. Specifically, we are employing generative models 
(recently GANs but also exploring explicit generative models) to deblend superpositions of galaxies in images taken by near-future large surveys 
such as LSST. We are also building fully probabilistic autoregressive generative models for quasar spectra in order to predict intrinsic quasar 
continua near Lyman-alpha to study the early universe IGM (e.g. by estimating the column density of neutral hydrogen.) 
