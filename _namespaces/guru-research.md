---
layout: namespace
title:  guru-research
date: 2019-05-28
name: guru-research
pi: Gary Cottrell
institution: University of California, San Diego
software: Python, CUDA, Keras, TensorFlow, Caffe
tagline: Applications of deep learning to image and video processing
imagesrc: guru-research.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

Gary's Unbelievable Research Unit (GURU) is carrying out a wide range of projects using deep learning. We have projects using deep learning 
to track dolphins in video in collaboration with comparative ethologists, segmenting mouse cardiac MRI to speed animal research in heart 
disease in collaboration with the UCSD Medical School, mapping from small molecule NMR to a cluster space where similar molecular structures 
are near one another in the space to speed structure elucidation for natural products in collaboration with researchers at Scripps Institution 
of Oceanography, and our own projects in computational cognitive neuroscience modeling the human visual system using anatomical constraintsin order  
to explain how the visual system works.
