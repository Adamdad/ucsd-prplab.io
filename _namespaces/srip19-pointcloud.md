---
layout: namespace
title: srip19-pointcloud
date: 2019-05-22
name: srip19-pointcloud
pi: Nuno Vasconcelos
institution: University of California, San Diego
software: Python, CUDA, PyTorch, TensorFlow, Caffe, NumPy, OpenCV
tagline: Classification of point cloud data
imagesrc: srip19-pointcloud.png
categories: 
- "namespace" 
tags: []
---

Classification of point cloud data: Various point cloud datasets have been recently introduced in computer vision. This data 
is quite important for applications such as smart cars, which rely on LIDAR data and similar sensors to improve sensing 
performance over what is possible with just cameras. We will investigate techniques for object recognition, detection, and 
segmentation of this type of data, using deep learning. This project aims for both application and top-tier conference publication.
