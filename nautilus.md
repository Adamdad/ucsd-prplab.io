---
layout: default
title: Nautilus documentation
short: nautilus documentation
siteimage: nautilus.png
custom_css: cluster_map_styles
---

<div class="post">
<h2>{{ page.title }}</h2>
<p>
Nautilus is a HyperCluster for running containerized Big Data Applications. 
It is using  Kubernetes for managing and scaling containerized applications and Rook for automating Ceph data services.<br>
The following pages provide documentation for Nautilus cluster users and
administrators, information about namespaces and applications running in
them  and the cluster map.
</p>

<div class="border">
  <strong>You are ...</strong>
</div>
<br>
<p class="docrow">
<a href="/userdocs/start/toc-start/" class="sbutton">Cluster user</a> &nbsp;&nbsp;
<a href="/admindocs/start/toc-start/" class="sbutton">Cluster administrator</a> &nbsp;&nbsp;
<a href="/nautilus/namespaces" class="sbutton">Browsing namespaces</a> &nbsp;&nbsp;
<a href="/nautilus/cluster-map" class="sbutton">Wondering where is Nautilus? </a>
</p>
