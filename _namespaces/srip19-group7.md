---
layout: namespace
title: srip19-group7
date: 2019-05-22
name: srip19-group7
pi: Nuno Vasconcelos
institution: University of California, San Diego
software: Python, CUDA, PytOrch, TensorFlow, Caffe, NumPy, OpenCV
tagline: Compressed Deep Learning Networks
imagesrc: srip19-group7.png
categories: 
- "namespace" 
tags: []
---

Compressed Deep Learning Networks: The development of slim and accurate deep neural networks has become crucial for real-world 
applications, especially or those employed in embedded systems like drones and smart phones. We are building light models, capable 
of making deep learning deployable in real-time on low-computation environments. These models will be used to build object recognition systems.
