---
layout: namespace
title:  neuralmtpp
date: 2019-01-14
name: neuralmtpp
pi: Christian Shelton
institution: University of California, Riverside
software: Python, CUDA, PyTorch, NumPy, CometML, Conda
tagline: Modified Continuous-time Long Short Term Memory Network
imagesrc: neuralmtpp.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

Learn statistical models of timings, values, and the heterogenous types of
events found in a medical event data from intensive care units.  Using a
modified continuous-time Long Short Term Memory network, we empirically
evaluate the efficacy of jointly learning the sequence distribution and
optimizing for a target cost at the end of the sequence. Our evaluation
is performed on a large cohort of patients on four clinically relevant
tasks, which requires powerful GPU computation.
