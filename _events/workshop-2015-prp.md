---
layout: page
title: Building the Pacific Research Platform
dates: 2015 October 14-16 
location: UCSD, La Jolla, CA
eventurlNO: /prp-workshop-2015
date: 2015-10-14
categories:
- "workshop"
- "2015"
siteimage: events.png
---

##### Wednesday 10/14/2015
<iframe width="560" height="315" src="https://www.youtube.com/embed/8rqhAuqAY8M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
##### Thursday 10/15/2015
<iframe width="560" height="315" src="https://www.youtube.com/embed/SlWMgOg_Mys" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
##### Friday 10/16/2015
<iframe width="560" height="315" src="https://www.youtube.com/embed/EU321fb2Uh0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

