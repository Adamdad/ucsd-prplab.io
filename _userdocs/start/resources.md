---
layout: collection-userdocs
title: Available Resources
date: 2019-01-10
short: userdocs
categories: user
order: 50
---

Although you can run your own containers, there are several services and resources 
already deployed by cluster admins that you can use without creating those yourself.

- [JupyterLab][1] 
- [Drone Images stitching: WebODM (Web Open Drone Map)][2]
- [Code and containers repository: GitLab][3]
- [File sharing: Nextcloud][4]
- [Network monitoring: traceroute tool][5]

[1]: https://jupyterhub.nautilus.optiputer.net/
[2]: https://webodm.nautilus.optiputer.net/
[3]: https://gitlab.nautilus.optiputer.net/
[4]: https://nextcloud.nautilus.optiputer.net/
[5]: https://traceroute.nautilus.optiputer.net/