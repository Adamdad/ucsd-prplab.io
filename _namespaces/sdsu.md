---
layout: namespace
title:  sdsu
date: 2019-01-14
name: sdsu
pi: Christopher Paolini
institution: San Diego State University
software: Python, CUDA, Keras, TensorFlow, Caffe
tagline: Fall Prediction and Detection through Inferencing at the Edge
imagesrc: sdsu.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

Among older adults, falls are the leading cause of injury-related morbidity and mortality. In 2015 29,000,000 falls were reported,
resulting in 33,000 deaths. Imperial County is home to 22,000 adults age 65 and over; 80% are Hispanic.  In 2013, falls were the second leading
cause of unintentional injury deaths in Imperial County, and 657 people were hospitalized due to falls that year. Falls were the most common
cause of non-fatal, unintentional injury hospitalizations, with the majority occurring in adults age 65 and over.
<br><br>
We propose to develop a wireless, wearable, low-power fall detection sensor to predict an imminent fall, and detect when a fall occurs, for
elderly Latinos.  The sensor will continuously acquire quantitative metrics of physical activity (duration of walking, standing, sitting,
and lying) and use this data to assess fall risk in Latino populations.
