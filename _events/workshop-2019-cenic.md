---
layout: page
title: The Right Connection - CENIC 2019                             
date: 2019-3-20
dates: 2019 March 18-20
location: La Jolla, CA
siteimage: events.png
imagesrc: 2019Logo.gif
categories: 
- "workshop"
- "2019"
tags: []
---

#### Annual Conference March 18-20, 2019

The Right Connection | CENIC 2019 brings together CENIC's richly diverse community, with participants from all education segments, including public and private research universities; public libraries; scientific, cultural, and performing arts institutions; private sector technology businesses; public policy and government; and R&E partners from across the country and around the world.
Please join us in San Diego to discuss and learn new ways that networks can support leading-edge research and create rich educational opportunities for all of our communities.

<a href="https://events.cenic.org/march-2019" class="button">CENIC Annual Conference Website</a>
<a href="https://events.cenic.org/march-2019/registration-2019" class="button">Registration</a>

