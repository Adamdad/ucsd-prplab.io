---
layout: page
title: Ceph Workshop             
date: 2020-1-22
dates: 2020 April 22
event_start_date: 20200422
location: Santa Clara, CA
eventurl: "https://cephworkshop2020.eventbrite.com/"
siteimage: events.png
imagesrc: computer.jpg
categories: 
- "event"
- "2020"
tags: []
---

#### Ceph Workshop: Exploring distributed open-source storage for high-volume, high-velocity research data

Join the Center for Information Technology Research in the Interest of Society (CITRIS) and the Banatao Institute, UCSC’s Center for Research in Open Source Software (CROSS), and the Pacific Research Platform (PRP) for Ceph Day at UC Santa Cruz, Silicon Valley Campus.

Attendees will get an overview of Ceph - what it is and what it’s for, as well as a discussion of cutting-edge Ceph use cases, followed by a hands-on tutorial to become familiar with using Ceph in a Kubernetes-orchestrated cluster. Participants will also learn how to use the basic data access model associated with Amazon S3 API. Audience members should bring their own laptops, but no other specialized skills or equipment are required.

Ceph is a free-software storage platform that supports object-, block- and file-level storage on distributed computing clusters. Ceph was designed to run on commodity hardware, which makes building and maintaining petabyte-scale data clusters economically feasible. Ceph has become the de facto storage solution for large-scale distributed computing clusters, such as CHASE-CI’s Nautilus Cluster (NSF Award #1730158).

Come learn about the scientific and research uses of Ceph on the Pacific Research Platform (NSF Award #1541349) and how it can benefit you and your research!

<a href="https://www.eventbrite.com/e/ceph-workshop-exploring-distributed-open-source-storage-for-research-data-tickets-87289403957" class="sbutton" >REGISTER HERE</a>
