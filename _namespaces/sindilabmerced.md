---
layout: namespace
title: sindilabmerced
date: 2019-05-29
name: sindilabmerced
pi: Suzanne Sindi
institution: University of California, Merced
software:  Keras, TensorFlow, PyTorch, Scikit-learn
tagline:  Feed Forward Neural Network
imagesrc: sindilabmerced.png
categories: 
- "namespace" 
tags: []
---

This project uses Nautilus to build a Feed Forward Neural Network  that would
predict the outcome of breast Cancer patient's treatment given their genetic mutation profile. 
Nautilus was used to shortened the training time by 700%.

