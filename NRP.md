---
layout: page
title: National Research Platform - Pilot
short: nrp
siteimage: nrp.jpg
---

<a href="https://meetings.internet2.edu/2019-924-third-national-research-platform-workshop/" target="_blank"><img src="/images/events/3nrp_date.png" style="width:400px"></a><br>
**Third National Research Platform Workshop<br>
Minneapolis, Minnesota September 24-25, 2019<br>**

Event materials coming soon

**<a href="https://spaces.at.internet2.edu/display/NRPWG/National+Research+Platform+%28NRP%29+Space">National Research Platform (NRP) Space - WIKI</a>**

**For more information on the National Research Platform please contact <a href="mailto:dbrunson@internet2.edu?subject=Re: National Research Platform">Dana Brunson</a>.**
