---
layout: page
title:  GRP Workshop 2019                  
date: 2019-9-18
dates: 2019 September 17-18
event_start_date: 20190917
location: La Jolla, CA
siteimage: events.png
eventurl: http://grp-workshop-2019.ucsd.edu
imagesrc: 2019grp_save.png
categories: 
- "workshop"
- "2019"
tags: []
---

#### The Global Research Platform Workshop - La Jolla, CA

Registration is now open for the Global Research Platform (GRP) Workshop, to be held 17-18 September 2019 at University of California San Diego. The website provides detailed information on the program, the venue and hotels in the vicinity. Registration is US $200 through August 15, 2019, and is US $300 starting August 16. The GRP Workshop has a number of informed keynote talks and panel sessions to stimulate discussions among attendees on a variety of topics, including application drivers, cyberinfrastructure, distributed data fabrics, data movement services, programmable networking, next-generation optical networking developments, and introductions to some of the evolving research platform initiatives taking place worldwide. The goal of this Workshop is to bring together network researchers, engineers and managers as well as application scientists to share best practices and advance the state of the art.

<a href="http://grp-workshop-2019.ucsd.edu/">Click here for more info</a>.




