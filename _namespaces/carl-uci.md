---
layout: namespace
title: carl-uci
date: 2019-01-14
name: carl-uci
pi: Jeff Krichmar
institution: University of California, Irvine
software: Python, CUDA, Pytorch, OpenAI gym, Conda, TensorBoard
tagline: Reinforcement learning and motion decomposition
imagesrc: carl-uci2.png
categories: 
- "namespace" 
tags: [3D modeling]
---

<b>Neuromodulation for learning and acting of the reinforcement learning algorithms:</b>
We are trying to apply the mechanisms in neuromodulation to  reinforcement learning and use the signals from neuromodulation 
to modulate the learning and acting of the reinforcement learning algorithms. 
<br><br>
<b>Motion Decomposition in Dynamic Scenes:</b>
This project focuses on learning continuous egomotion representation from monocular video and 
pose information. The trained model infer ego motion parameters and object velocities for unseen videos.
<br>

