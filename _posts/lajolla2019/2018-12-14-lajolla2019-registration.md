---
layout: workshop
title: "La Jolla FIONA Workshop Registration "
date: 2019-02-14
workshop: lajolla2019
short: lajolla2019-registration
img: lajolla2019/header.png
---

<div class="font-weight-bold text-info" id="participation">To Participate in the Workshop</div>
- <b>Space in the workshop will be limited to 20 participants</b>. 
The first 20 nominations will be enrolled in the workshop; a short waiting list
(5 additional prospective participants) will be kept, as well. Participants will be
contacted to confirm enrollment, logistics, and other workshop details.
- Participants will be nominated by their campus CIO who will email Kao Saefong, Executive Assistant to Louis Fox at
ksaefong@cenic.org. 
- Each campus CIO may nominate one participant from their institution. 

<div class="font-weight-bold text-info" id="audience">Target Audience</div>
Operational staff with responsibility for science engagement, e.g.:<br>
    - Cyberinfrastructure Engineers<br>
    - High-Performance Computing Specialists<br>
    - Research Systems Administrators<br>
    - System Integration Engineers


<div class="font-weight-bold text-info" id="prerequisites">Prerequisites </div>
Experience with a UNIX/Linux command line environment
<br>Basic understanding of TCP/IP and UDP protocols
<br>Basic UNIX/Linux System Administration skills and familiarity with Linux PC server hardware
<br>Basic Network Engineering skills and familiarity with modern routing and switching technologies 
<br><b>Please note: participants are required to bring a laptop computer</b>

[Back to top]({{page.url}})

