---
layout: workshop
title: "La Jolla Kubernetes-Grafana  Workshop"
date: 2019-02-19
workshop: lajolla2019k8s
short: lajolla2019k8s-overview
img: lajolla2019k8s/k8s.png
---

<table class="info">
<tr>
<td width="50%">
<font class="font-weight-bold text-info" >Date: </font>Sunday, March 17, 2019<br>
<font class="font-weight-bold text-info" >Location: </font> 
<font style="Color:red">Room 5302 (5th floor)</font> Atkinson Hall, <br>UCSD, La Jolla, CA 92093 <br>
</td>
<td>
<font class="font-weight-bold text-info">Sponsors:</font> PRP and CENIC<br>
<font class="font-weight-bold text-info" >Chair: </font>John Graham <br>
<font class="font-weight-bold text-info" >Instructors: </font>Dima Mishin & Nadya Williams <br>
</td>
</tr>
</table>
<b>Note: </b>For directions to the Atkinson Hall Building see
[Google map](https://www.google.com/maps/place/Atkinson+Hall+Auditorium/@32.8816236,-117.2361272,17z/data=!4m5!3m4!1s0x80dc06c31a137e61:0xe89cabc0c3b3ef18!8m2!3d32.8823548!4d-117.2342556)
<div class="font-weight-bold text-info" id="instructors">Overview</div>
In this workshop, the PRP  Kubernetes and Grafana team will show participants beginner and advanced features and in-depth examples of using:<br>
- <b>Kubernetes</b>, an open-source orchestrator for containerized applications<br>
- <b>Grafana</b>, open platform for creating analytical and monitoring dashboards<br>
- <b>Prometheus</b>, monitor for Kubernetes services 


[Back to top]({{page.url}})

