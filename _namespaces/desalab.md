---
layout: namespace
title: desalab
date: 2019-06-09
name: desalab
pi: Virginia de Sa
institution: University of California, San Diego
software: Docker, TensorFlow, TensorBoard, Keras, NumPy, SciPy
tagline: Biologically plausible deep learning for vision
imagesrc: desalab.png
categories: 
- "namespace" 
tags: []
---

Computational modeling of early vision: We are working on developing a unified and complete computational model of the early 
visual cortex that simultaneously explains behavioral data and in-vivo neural activity data collected from the primate striate cortex. 
We are currently developing biologically plausible deep neural network models equipped with long-range horizontal connections and 
applying them to learn early-visual tasks like boundary detection.
