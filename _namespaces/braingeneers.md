---
layout: namespace
title: braingeneers
date: 2019-05-29
name: braingeneers
pi: David Haussler
institution: UC  Santa Cruz & UC San Francisco
software:  Python, TensorFlow, Jupyter
tagline: Machine Learning with the recordings derived from cortical organoids 
imagesrc: braingeneers.png
categories: 
- "namespace" 
tags: []
---

The Braingeneers are developing the infrastructure to grow cortical organoids at scale and interface with them in order to record 
and stimulate neurons. This will enable the application of modern AI approaches to uncover how genetic changes enhanced human 
brain architecture and computing capacity during primate evolution as well as to better understand how neurons function towards back porting 
this into in-silico machine learning models. Currently we are leveraging the PRP's Kubernetes cluster to run machine learning algorithms on 
the recordings derived from these organoids as well as the CEPH S3 storage to host the data internally and make it available externally so 
that researchers and students around the world will be able to analyze the data using Jupyter notebooks. We are also starting to leverage 
the PRP directly from the Rasberry Pi's that control the organoid systems in order to directly upload recordings and imaging for processing and analysis.

