---
layout: namespace
title: domain-adaptation
date: 2019-05-22
name: domain-adaptation
pi: Nuno Vasconcelos
institution: University of California, San Diego
software: Python, CUDA, PyTorch, TensorFlow, Caffe, NumPy, OpenCV
tagline: Transfer and multitask learning 
imagesrc: domain-adaptation.png
categories: 
- "namespace" 
tags: []
---

Domain Adaptation: We are investigating methods for transfer learning and multi-task learning. The target is to use a well 
pre-trained model for other tasks with or without supervision and build the bridge between different domains. Currently, 
we focus on the problem about unsupervised domain adaptation for semantic segmentation and multi-domain learning for several classification datasets.
