---
layout: namespace
title: ucsd-haosulab
date: 2019-01-14
name: ucsd-haosulab
pi: Hao Su
institution: University of California, San Diego
software: PyTorch, Conda
tagline: Machine learning methods based on point cloud
imagesrc: ucsd-haosulab.png
categories: 
- "namespace" 
tags: [3D modeling]
---

Hao Su's lab is working on refinement and acceleration of machine learning methods based on point cloud, 
like PointNet and its variants. The improved efficiency and accuracy will help many real-world 
applications including self-driving car.
