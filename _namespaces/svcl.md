---
layout: namespace
title: svcl
date: 2019-05-22
name: svcl
pi: Nuno Vasconcelos
institution: University of California, San Diego
software: Python, CUDA, PyTorch, TensorFlow, Caffe, NumPy, OpenCV
tagline: Synthetic data augmentation
imagesrc: svcl.png
categories: 
- "namespace" 
tags: []
---

Data collection in the real world is very expensive. However, there are infinite sources of synthetic data from gaming 
environments, which are very easy/cheap to collect. We are exploring the impact of synthetic data on the training of 
real-world computer vision systems. This involves collecting a large amount of synthetic data from game engines, training 
vision models with this data, and measuring their performance in real-world computer vision tasks, e.g. object detection.
