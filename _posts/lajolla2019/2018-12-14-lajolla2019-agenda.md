---
layout: workshop
title: "La Jolla FIONA  Workshop Agenda"
date: 2018-12-14
workshop: lajolla2019
short: lajolla2019
img: lajolla2019/header.png
---

<a href="#day1" class="subtitle">March 16</a> | 
<a href="#day2" class="subtitle">March 17</a>

<table class="info">
<tr><td width="25%">
<div class="font-weight-bold text-info" id="instructors">Instructors</div>
Hervey Allen (HA), <a href="https://nsrc.org/">NSRC</a><br>
John Hess (JH), <a href="https://cenic.org/">CENIC</a><br>
Isaac Nealey (IN), <a href="https://www.ucsd.edu/">UCSD</a><br>
Joel Polizzi (JP), <a href="https://www.ucsd.edu/">UCSD</a><br>
Tom DeFanti (DF), <a href="https://www.ucsd.edu/">UCSD</a><br>
</td>
<td>
<div class="font-weight-bold text-info" id="notes">Notes</div>
Particularly as the PRP FIONA Workshop is itself a collaborative development effort, the program
content, session/lab schedule, and instructors are subject to change. We expect the general flow and
cadence of the workshop to be as described below.<br>
All times are PDT (Pacific Daylight Time)  UTC/GMT - 7 hours
</td>
</tr>
</table>

<b>NOTE: </b>
<br> Breakfast will be served 8-9AM.  Please be on time as class starts sharply at 9AM.
<br> <font style="color:red">Catering will be provided on-site for Breakfast,
Lunch and Breaks on the 5th floor </font>

<h4>Workshop Status</h4>

<b>Host status:</b> <a href="http://tools.nsrc.org/nagios3/">Nagios</a> (user: guest, pass: guest)

<div class="font-weight-bold text-info" id="day1">Saturday, March 16 </div>

<table class="workshop">
<tr>
    <th>Time</th>
    <th>Activity</th>
</tr>
<tr>
  <td class="break">08:00 - 09:00</td>
  <td class="break">Breakfast</td>
</tr>
<tr>
  <td>09:00 - 09:30</td>
  <td>Workshop Welcome (JH)
  <ul>
     <li>FIONette initial login (downloads: <a
   href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/systems/connecting-linux.md">Initial Connection</a>)</li>
     <li>Workshop Rocket.Chat channel <a href="https://rocket.nautilus.optiputer.net/channel/lajolla-tutorial">lajolla-tutorial</a></li>
     <li>Staging to run Jupyter Notebooks for selected lab exercises <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/ipynb/staging-for-jupyter-lab.md">HTML(md)</a></li>
  </ul>
  </td>
</tr>
<tr>
  <td>09:30 - 10:30</td>
  <td><b>Session 1:</b> Network Performance Measurement Concepts: perfSONAR (HA)
  (downloads: <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/raw/master/perfsonar/perfsonar-introduction.pdf">PDF</a> <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/raw/master/perfsonar/perfsonar-introduction.pptx">PowerPoint</a>)
   <ul>
     <li>Jitter</li>
     <li>Types of Delay and their impact</li>
     <li>perfSONAR what it is</li>
     <li>perfSONAR ecosystem and what we are using</li>
     <li>How to measure reliability and throughput</li>
   </ul>
   <b>Session 2:</b> Explanation of our machine setup (HA) <br>
   <b>Lab 1:</b> CentOS Orientation and Configuration (downloads: <a
   href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/systems/centos-initial-configuration.md">Lab1</a>) (HA)
   </td>
</tr>
<tr>
  <td class="break">10:30 - 10:45</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>10:45 - 12:30</td>
  <td>
   <b>Lab 2:</b> perfSONAR Lab (Install perfSONAR Testpoint Bundle) (downloads: <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/perfsonar/exercises-perfsonar-testpoint.md">Lab2</a>) (HA) <br>
   <b>Lab 3:</b> Command line ad hoc network performance testing with perfSONAR tools (downloads: <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/perfsonar/exercises-perfsonar-testpoint-bundle-use.md">Lab3</a>) (HA) <br>
   <b>Lab 4:</b> (optional) SSH: Configure SSH Keys on FIONette (downloads: <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/security/exercises-perfsonar-ssh-key.md">Lab4</a>) (HA)
  </td>
</tr>
<tr>
  <td class="break">12:30 - 13:30</td>
  <td class="break">Lunch</td>
</tr>
<tr>
  <td>13:30 - 14:00</td>
  <td><b>Session 3:</b> GridFTP: Presentation and Lab (Install GridFTP)  (JH) <br>
    <b>Lab 1:</b> Install GridFTP and esmond-client (downloads: <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/GridFTP/lajolla-3.2--gridftp--esmond-client.md">Lab 3.1</a>)
  </td>
</tr>
<tr>
  <td>14:00 - 15:00</td>
  <td><b>Session 4:</b> MaDDash: Presentation and Lab (Install MaDDash and Central Measurement Archive) (JH) <br>
    <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/maddash--ma/PRP-FIONA-Workshop-LaJolla-MaDDash-jhess-final.pdf"> Monitoring and Debugging Dashboard, and Measurement Archive (PDF)</a><br>
    <b>Lab 1:</b> Install perfsonar-centralmanagement (downloads: <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/maddash--ma/lajolla-4.1--perfsonar-centralmanagement.md">Lab 4.1</a>)<br>
  
  </td>
</tr>
<tr>
  <td class="break">15:00 - 15:15</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>15:15 - 17:00</td>
  <td><b>Session 5:</b> perfSONAR and GridFTP mesh configuration: Presentation and Lab (Registering results, MaDDash visualization) (JH) <br>
     <b>Lab 1:</b> per-group perfSONAR and GridFTP mesh integration configuration (downloads: <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/per-group-mesh-integration/lajolla-5.1--per-group-mesh-ps-gridftp.md">Lab 5.1</a>)<br>
  </td>
</tr>
</table>

[Back to top]({{page.url}})
<br>

<div class="font-weight-bold text-info" id="day2">Sunday, March 17 </div>

<table class="workshop">
<tr>
    <th>Time</th>
    <th>Activity</th>
</tr>
<tr>
  <td class="break">08:00 - 09:00 </td>
  <td class="break">Breakfast</td>
</tr>
<tr>
  <td>09:00 - 10:30</td>
  <td><b>Session 1:</b> Dashboard Roundup: Diagnose workshop testing and visualization environment;
      Identify & resolve issues affecting regular testing, registering results, visualization (JH)
  <br>
      <b>Complete</b> <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/per-group-mesh-integration/lajolla-5.1--per-group-mesh-ps-gridftp.md">Lab 5.1</a> (JH)</td>
</tr>
<tr>
  <td class="break">10:30 - 10:45</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>10:45 - 12:00</td>
  <td><b>Session 2:</b> CentOS Basics (HA) (downloads: <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/raw/master/systems/centos-basics.pdf">PDF</a> <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/raw/master/systems/centos-basics.pptx">PowerPoint</a>) 
  <br>
  <b>Session 3:</b> SSH Key Refresher (HA) (downloads: <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/raw/master/security/ssh-keys.pdf">PDF</a> <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/raw/master/security/ssh-keys.pptx">PowerPoint</a>)
  <br>
  <b>Session 4:</b> Science DMZ Architecture, Data Movement, DTNs and Security (HA) (downloads: <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/raw/master/security/perfsonar-security.pdf">PDF</a> <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/raw/master/security/perfsonar-security.pptx">PowerPoint</a>)
  <br>
  <b>Lab 1:</b> Host Security with perfSONAR Testpoint Bundles and CentOS 7 (Download: <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/security/exercises-perfsonar-security.md">Lab 1</a>)
  </td>
</tr>
<tr>
  <td class="break">12:00 - 13:00</td>
  <td class="break">Lunch</td>
</tr>
<tr>
  <td>13:00 - 14:15</td>
  <td><b>Session 5:</b> Reviewing measurement results (JH)
  <br>
  <b>Session 6:</b> Sun Cave Excursion and Big Data (IN, JP, TD) at 13:30 PM</td>
</tr>
<tr>
  <td class="break">14:15 - 14:30</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>14:30 - 16:00</td>
  <td><b>Session 7:</b> Performance measurement and visualization deployment planning (JH)
  <br>
      <b>Engineering Roundtable</b> (everyone)
  </td>
</tr>
</table>

<br>

<h3>References</h3>
<ul>
   <li>Editor: vi cheat sheet (<a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/references/vi-cheat-sheet.pdf">PDF</a>)</li>
   <li>Editor: Nano cheat sheet (<a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/references/Nano_Cheat_Sheet.pdf">PDF</a>) (thanks to <a href="http://www.cheat-sheets.org/">www.cheat-sheets.org</a>)</li>
   <li>System: Yum commands (<a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/references/rh_yum_cheatsheet_1214_jcs_print-1.pdf">PDF</a>)</li>
   <li>System: Comparison of NTP implementations (Chrony) (<a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/raw/master/references/comparison.html?inline=false">Download HTML</a>)</li>
</ul>

<br>
[Back to top]({{page.url}})
