<div class="data-table-container">
  <table class="data-table">
    <tr>
      <th class="hostname">Hostname</th>
      <th>IP</th>
      <th>CPUs</th>
      <th>GPUs</th>
      <th>Memory</th>
      <th>Storage</th>
      <th>Network</th>
      <th>State</th>
      <th>City</th>
    </tr>
    {% for node in site.data.cluster_map.nodes %}
      <tr>
        <td class="hostname">{{ node.hostname | replace: ".", "<wbr>." }}</td>
        <td>{{ node.ip }}</td>
        <td>{{ node.cpus }}</td>
        <td>{{ node.gpus }}</td>
        <td>{{ node.memory }}</td>
        <td>{{ node.storage }}</td>
        <td>{{ node.network }}</td>
        <td>{{ node.state }}</td>
        <td>{{ node.city }}</td>
      </tr>
    {% endfor %}
  </table>
</div>
