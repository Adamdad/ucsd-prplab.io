---
layout: namespace
title:  srinjoy-keras
date: 2019-01-14
name: srinjoy-keras
pi: Alex Cloninger
institution: University of California, San Diego
software: PyTorch, TensorFlow, Keras, Conda
tagline: Deconvolutional Networks Sparsity study
imagesrc: srinjoy-keras.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

Our project aims to study different schemes to achieve sparsity for Deconvolutional Networks trained as GANs (Generative Adversarial Networks) 
which is critical for efficient implementation on realtime processing platforms such as GPUs and FPGAs. The goal of the project is to explore 
novel methods to achieve sparsity using a network distillation approach under pre-specified performance constraints using both generated data 
(pgen) as well as data used to originally train the GAN (pdata). Empirical results of this work will provide critical inputs for theory 
development in the context of whether it is advantageous to use pdata or pgen.
