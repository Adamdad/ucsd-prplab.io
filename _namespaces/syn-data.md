---
layout: namespace
title: syn-data
date: 2019-05-28
name: syn-data
pi: Nuno Vasconcelos
institution: University of California, San Diego
software: Python, Pytorch, MatterPort3D, Habitat, Gibson, TensorboardX
tagline: Vision-and-Language Navigation
imagesrc: syn-data.png
categories: 
- "namespace" 
tags: []
---

Although vision and language methods have made significant progress, it remains challenging to integrate these heterogeneous methods for general 
tasks.  We are investigating Vision-language Navigation (VLN) task, in which a robot in an unseen environment is navigated through natural language.  
We will build an effective method for VLN tasks using a virtual environment which provides photo-realistic graphics inside buildings.  This project 
aims for both application and top-tier conference publication.
