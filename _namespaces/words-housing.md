---
layout: namespace
title: words-housing
date: 2019-05-20
name: words-housing
pi: Volkan Vural
institution: University of California, San Diego
software: Python, Conda, TensorFlow
tagline: Machine Learning to predict property values in San Diego County
imagesrc: words-housing.png
categories: 
- "namespace" 
tags: []
---

Developing Machine Learning based techniques to predict the property values in San Diego County. These techniques are being developed 
using data obtained from multiple sources. The data can be categorized as: 
<br>
<b>1. Intrinsic property features</b> such as number of bedrooms, bathrooms, square footage, etc.<br>
<b>2. External features</b> such as school districts and school ratings. <br>
<b>3. Historical transactions</b> such as sales, valuations, foreclosures. <br>
Property characteristics, address details, sales, valuation, foreclosure datasets span a 34 year period from 1987 to 2017 and were 
collected from City of San Diego whereas other data sets such as school districts, school ratings are available online. Using these 
datasets, more than 250 features were engineered but only 37 features that offered significant predictive power were selected to 
feed into the prediction system. In order to further improve the performance of the developed predictive system, more external variables 
are being analyzed and incorporated to the system. These external variables include but not limited to the macroeconomic indicators 
such as mortgage rates, libor, prime rates, inflation, census data, USD to MXN (Mexican Peso) exchange rate and etc. Additionally, 
the cross-border economic activity and cultural identity on the housing market in San Diego will also be investigated. This could be the 
first study on cross-border economic effects on housing prices using Machine Learning techniques in the literature. This project also serves 
as a capstone for the grad students at MAS
