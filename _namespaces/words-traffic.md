---
layout: namespace
title: words-traffic
date: 2019-05-20
name: words-traffic
pi: Volkan Vural
institution: University of California, San Diego
software: Python, Conda, TensorFlow
tagline: Machine Learning to analyze and predict traffic patterns
imagesrc: words-traffic.png
categories: 
- "namespace" 
tags: []
---

A large traffic dataset joined with environmental data is explored to predict the changes in traffic patterns caused by environmental 
factors and events such as Padres games, festivals, farmers markets, marathons, sports events and etc. Event based traffic prediction 
is not only an improvement to existing commercial products but also crucial for many applications including evacuation planning and 
routing optimization. Due to the nature of the project, large data sets coming from multiple data sources are joined and analyzed using 
high-performance computing resources at CHASE-CI. Machine learning techniques are used to analyze and predict the traffic with respect 
to various events. This project is ongoing and currently being extended to the following fields: <br>
1. Estimating traffic conditions and planning evacuation routes during/after wildfires, <br>
2. Joining weather station data to predict the impacts of different weather conditions on traffic,<br>
3. Joining California Highway Patrol data to predict the traffic patterns after an accident, Analysis of how traffic patterns 
   impact the housing market in San Diego County.<br>

