---
layout: namespace
title:  ecdna
date: 2019-01-14
name: ecdna
pi: Vineet Bafna
institution: University of California, San Diego
software: Python, Conda, Keras, TensorFlow, SciPy, Scikit-learn, OpenCV
tagline: Deep Learning for medical imaging
imagesrc: ecdna.png
categories: 
- "namespace" 
tags: [Kubernetes]
---

We use deep learning and computer vision techniques to explore 2D medical images of tumor cells. 
In particular, we quantify proteins and DNA segments in metaphase images of normal and tumor cells 
that offer breakthrough insights on the persistence and proliferation of cancer.

