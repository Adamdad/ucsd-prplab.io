---
layout: page
title: People
short:  
siteimage: news.png
---
<div class="box">
<span class="image fit"><img src="/images/PRP_PIs.jpg"/></span>
<h3>PRP Leadership Team</h3>
    <p>
        The Pacific Research Platform leadership team includes faculty from two of the multi-campus Governor Gray Davis Institutes of Science and Innovation created by the State of California in 2000: Calit2, and the Center for Information Technology Research in the Interest of Society (CITRIS), led by UC Berkeley. “The Pacific Research Platform is an ideal vehicle for collaboration between CITRIS and Calit2 given the growing importance of universities working together for the benefit of society,” said CITRIS Deputy Director Camille Crittenden, co-PI on the PRP award. “The project also received strong support from members of the UC Information Technology Leadership Council, which includes chief information officers from the 10 UC campuses, five medical schools, the Lawrence Berkeley National Lab and the Office of the President.” Crittenden will manage the science engagement team and the enabling relationships with CIOs on participating campuses and labs.
<p>                                
<a href="http://lsmarr.calit2.net">Larry Smarr, Director, Calit2</a><br>
<a href="http://citris-uc.org/person/camille-crittenden/">Camille Crittenden, Deputy Director, CITRIS</a><br>
<a href="https://directory.uci.edu/index.php?uid=ppapadop&return=philip">Philip Papadopoulos, Program Director, UC Computing Systems</a><br>
<a href="http://www.calit2.net/people/staff_detail.php?id=67">Thomas DeFanti, Research Scientist, Calit2</a><br>
<a href="https://www-physics.ucsd.edu/fac_staff/fac_profile/faculty_description.php?person_id=494">Frank Würthwein, Physicist, UC San Diego; Program Director, SDSC</a></p>                                   
</p>