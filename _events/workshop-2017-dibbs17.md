---
layout: page
title: DIBBs17 Workshop
date: 2017-01-11
dates: 2017 January 11-12
location: Arlington, VA
siteimage: dibbs17.png
categories:
- "workshop"
- "2018"
---

[The First NSF PI Data Infrastructure Building Blocks workshop January 11 - 12, 2017, Arlington, Virgina][dibbs17]

The 1st NSF Data Infrastructure Building Blocks PI Workshop (DIBBs17) was held January 11-12, 2017, at the Westin Arlington Gateway Hotel in Arlington, VA . This invitation-only event, was limited to 2 attendees (PI or Co-PI) per DIBBs project that was active at the time of invitation. The goals of the workshop were to exchange results and lessons learned from the ~40 active projects that had been funded through DIBBs solicitations (NSF 12-557, NSF 14-530, NSF 15-534, and NSF 16-530), and consider the implications of project results across the existing research portfolio for advances in the visions and goals for data cyberinfrastructure. Prior to the workshop, each project provided a poster and white paper. The Program Committee used these inputs to form the panels, small group discussions, and Poster Reception. All active DIBBs projects were encouraged to participate in DIBBs17.

<a href="https://dibbs17.org/" class="button">DIBBs17 Website</a>

[dibbs17]: https://dibbs17.org/

